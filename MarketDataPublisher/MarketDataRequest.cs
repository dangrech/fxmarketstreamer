﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketDataPublisher
{
    public enum MarketDataRequestType
    {
        SUBSCRIBE = 1,
        SUBSCRIBE_TO_ALL_FROM_SOURCE = 2,
        UNSUBSCRIBE = 3,
        UNSUBSCRIBE_ALL = 4,
        SOURCE_SNAPSHOT = 5
    }
    public class MarketDataRequest
    {
        public MarketDataRequestType Type { get; set; }
        public List<String> Currencies { get; set;  }
    }
}
