using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Threading.Tasks;
using Models.Financial;
using MarketDataPublisher;
using Services;
using FixEngineIngestion;

namespace RedisDataPersistor
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart()
        {
            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.
            Task.Factory.StartNew(StartPersisting);

            return base.OnStart();
        }

        public void StartPersisting()
        {
            
        }
    }
}
