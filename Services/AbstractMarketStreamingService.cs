﻿using Com.Lmax.Api.OrderBook;
using MarketDataHandler.Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public abstract class AbstractMarketStreamingService
    {
        protected List<Models.Financial.Instrument> subscribedInstruments;
        protected Action<MarketTick> onMarketData;
        protected Action<ConnectionState> onStateChange;

        public AbstractMarketStreamingService(Action<MarketTick> onMarketData, List<Models.Financial.Instrument> subscribedInstruments, Action<ConnectionState> onStateChange)
        {
            this.onMarketData = onMarketData;
            this.subscribedInstruments = subscribedInstruments;
            this.onStateChange = onStateChange;
        }

        public abstract void StartSession();


    }
}
