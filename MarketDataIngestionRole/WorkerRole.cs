using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using MarketDataHandler.Models.Financial;
using MessageBus;
using Models.Financial;
using DataIngestionRole;
using Services;

namespace MarketDataIngestionRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("MarketDataIngestionRole is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            catch(Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("MarketDataIngestionRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("MarketDataIngestionRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("MarketDataIngestionRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            IPEndPoint endpoint = RoleEnvironment.Roles["MessagingBusRole"].Instances[0].InstanceEndpoints["MessageBusPublish"].IPEndpoint;

            MessageBusClient<MarketTick> mbClient = new MessageBusClient<MarketTick>(endpoint.Address.ToString(), endpoint.Port, 100000000);

            mbClient.Connect();

            AbstractMarketStreamingService dataStreamer;

            if (RoleEnvironment.GetConfigurationSettingValue("MarketDataProvider") == "LMAX")
            {
                List<Instrument> symbolList = Util.GetMajorLMAXInstruments();

                dataStreamer = new LmaxMarketStreamingService(x => { mbClient.Publish(x); },
                    symbolList.Where(x => !x.isComputed).ToList(), newState =>
                    {
                        if (newState == ConnectionState.CONNECTION_OPEN)
                        {
                            Trace.TraceInformation("Lmax Connection Open");
                        }
                        else
                        {
                            Trace.TraceInformation("Lmax Connection Closed");
                        }
                    });
            }
            else
            {
                /* KGI */ 
                dataStreamer = new FixEngineIngestion.FixEngineStreamingService(x => { mbClient.Publish(x); }, Util.GetTaggKgiInstruments(), newState =>
                {
                    if (newState == ConnectionState.CONNECTION_OPEN)
                    {
                        Trace.TraceInformation("FIX Connection Open");
                    }
                    else
                    {
                        Trace.TraceInformation("FIX Connection Closed");
                    }
                });
            }

            dataStreamer.StartSession();

            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
