﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FIXForge.NET.FIX;
using FIXForge.NET.FIX.FIX43;
using System.Globalization;
using System.Diagnostics;
using MarketDataHandler.Models.Financial;
using FIXForge.NET.FIX.Scheduling;
using Models.Financial;

namespace FixEngineIngestion
{
    public class FixTradingClient : IDisposable
    {
        public Session session { get; set; }
        private SessionConfiguration settings;
        private Dictionary<string, MarketDataUpdate> lastMD;
        private int requestsCounter = 0;
        private Dictionary<string, Message> mdId2Request;
        private Action<MarketTick> onMarketData;
        private Scheduler scheduler;
        List<Instrument> insrumentSubscriptions;
        private Action<ConnectionState> onStateChange; 

        public FixTradingClient(SessionConfiguration settings, Action<MarketTick> onMarketData, Action<ConnectionState> onStateChange)
        {
            this.settings = settings;
            this.onMarketData = onMarketData;
            lastMD = new Dictionary<string, MarketDataUpdate>();
            mdId2Request = new Dictionary<string, Message>();
            this.onStateChange = onStateChange;

            session = new Session(settings.SenderCompID, settings.TargetCompID, settings.Version, settings.KeepSequenceNumbersAfterLogout, SessionStorageType.AsyncFileBasedStorage);
            session.StateChangeEvent += OnSessionStateChanged;
         

            if (settings.UseSslEncryption)
                session.Encryption = EncryptionMethod.SSL;
            else
                session.Encryption = EncryptionMethod.NONE;

            if (!string.IsNullOrEmpty(settings.SslCertificateFile))
            {
                session.Ssl.CertificateFile = settings.SslCertificateFile;
                session.Ssl.PrivateKeyFile = settings.SslCertificateFile;
            }

            session.InboundApplicationMsgEvent += new InboundApplicationMsgEventHandler(OnInboundApplicationMsg);
            
        }

        
         private void OnSessionStateChanged(object sender, StateChangeEventArgs args)
         {
            if (args.NewState == SessionState.DISCONNECTED)
            {
                onStateChange(ConnectionState.CONNECTiON_CLOSED);
            }

            else if (args.NewState == SessionState.ACTIVE)
            {
                onStateChange(ConnectionState.CONNECTION_OPEN);
            }

            else if (args.NewState == SessionState.LOGON_IN_PROGRESS || args.NewState == SessionState.LOGOUT_IN_PROGRESS || args.NewState == SessionState.RECONNECTING)
            {
                onStateChange(ConnectionState.CONNECTION_PENDING);
            }
         }
        

        private long GetMillisecondTimestamp()
        {
            DateTime staticDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - staticDate;
            return (long)timeSpan.TotalMilliseconds;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (session != null)
                {
                    session.Dispose();
                    session = null;
                }
            }
        }


        public void Connect(List<Instrument> instruments)
        {
            this.insrumentSubscriptions = instruments;

            Message customLogon = new Message(MsgType.Logon, session.Version);

            if (!string.IsNullOrEmpty(settings.Username))
            {
                customLogon.Set(Tags.Username, settings.Username);
            }

            if (!string.IsNullOrEmpty(settings.Password))
            {
                customLogon.Set(Tags.Password, settings.Password);
            }

            if (!string.IsNullOrEmpty(settings.ClientID))
            {
                customLogon.Set(Tags.ClientID, settings.ClientID);
            }

            if (!string.IsNullOrEmpty(settings.RawData))
            {
                customLogon.Set(Tags.RawData, settings.RawData);
                customLogon.Set(Tags.RawDataLength, settings.RawData.Length);
            }

            SessionSchedule regularSingleDay = new SessionSchedule(DayOfWeek.Sunday, DayOfWeek.Friday,
                                                                   new TimeSpan(21,05,00), new TimeSpan(20,55,00),
                                                                    SessionDuration.Day,
                                                                    SequenceNumberResetPolicy.Daily);

            InitiatorConnectionSettings sessionConnectionSetting = new InitiatorConnectionSettings(settings.Host, settings.Port, 30, true, customLogon) ;

            scheduler = new Scheduler();

            scheduler.SessionLoggedOn += Scheduler_SessionLoggedOn;
           
            scheduler.ReconnectAttempts = 1000000;
            scheduler.ReconnectInterval = 10;
            scheduler.UtcTimeUsage = true;
            scheduler.Register(session, regularSingleDay, sessionConnectionSetting);

        }

        private void Scheduler_SessionLoggedOn(object sender, SessionEventArgs e)
        {
            this.session = e.Session;

            if (e.Session.State == SessionState.ACTIVE)
            {
                    foreach (var instrument in insrumentSubscriptions)
                    {
                        try
                        {
                            if (instrument.isNDF)
                            {
                                SubscribeToNDF(instrument.Symbol, true, instrument.NDFPeriod);
                            }
                            else
                            {
                                Subscribe(instrument.Symbol, true);
                            }
                        }
                        catch (Exception ex)
                        {
                            Trace.WriteLine("Error when subscribing to " + instrument + " : " + ex.Message);
                        }
                }
            }
        }

        private string Subscribe(string symbol, bool requestUpdates)
        {
            while (SessionState.ACTIVE != session.State)
            {
                Trace.WriteLine("Cannot subscribe to the symbol '" + symbol + "' : the market data session is in " +
                                               session.State + " state. Waiting");
            }

            Message request = new Message(MsgType.Market_Data_Request, session.Version);

            request.Set(Tags.MDReqID, ++requestsCounter);
            request.Set(Tags.SubscriptionRequestType, requestUpdates ? SubscriptionRequestType.Snapshot__plus__Updates : SubscriptionRequestType.Snapshot);
            request.Set(Tags.MarketDepth, MarketDepth.Top_of_Book);
            request.Set(Tags.MDUpdateType, MDUpdateType.Incremental_Refresh);
            request.Set(Tags.AggregatedBook, "Y");

            Group entryTypes = request.SetGroup(Tags.NoMDEntryTypes, 2);
            entryTypes.Set(Tags.MDEntryType, 0, MDEntryType.Bid);
            entryTypes.Set(Tags.MDEntryType, 1, MDEntryType.Offer);
            
            Group relatedSymbols = request.SetGroup(Tags.NoRelatedSym, 1);
            relatedSymbols.Set(Tags.Symbol, 0, symbol);

            session.Send(request);

            lock (mdId2Request)
                mdId2Request.Add(requestsCounter.ToString(CultureInfo.InvariantCulture), request);

            return requestsCounter.ToString(CultureInfo.InvariantCulture);
        }

        public string SubscribeToNDF(string symbol, bool requestUpdates, String ndfPeriod)
        {
            if (SessionState.ACTIVE != session.State)
            {
                throw new ApplicationException("Cannot subscribe to the symbol '" + symbol + "' : the market data session is in " +
                                               session.State + " state");
            }

            Message request = new Message(MsgType.Market_Data_Request, session.Version);
            
            request.Set(Tags.MDReqID, ++requestsCounter);
            request.Set(Tags.SubscriptionRequestType, requestUpdates ? SubscriptionRequestType.Snapshot__plus__Updates : SubscriptionRequestType.Snapshot);
            request.Set(Tags.MarketDepth, MarketDepth.Full_Book);
            request.Set(Tags.MDUpdateType, MDUpdateType.Incremental_Refresh);
            request.Set(Tags.SecurityType, "FX_NDF");
            request.Set(9301, ndfPeriod);

            Group entryTypes = request.SetGroup(Tags.NoMDEntryTypes, 2);
            entryTypes.Set(Tags.MDEntryType, 0, MDEntryType.Bid);
            entryTypes.Set(Tags.MDEntryType, 1, MDEntryType.Offer);
            
            Group relatedSymbols = request.SetGroup(Tags.NoRelatedSym, 1);

            relatedSymbols.Set(Tags.Symbol, 0, symbol);

            session.Send(request);

            lock (mdId2Request)
                mdId2Request.Add(requestsCounter.ToString(CultureInfo.InvariantCulture), request);

            return requestsCounter.ToString(CultureInfo.InvariantCulture);
        }

        public void Unsubscribe(string requestID)
        {
            lock (mdId2Request)
            {
                if (!mdId2Request.ContainsKey(requestID))
                {
                    throw new ApplicationException("Unknown MD Request ID: '" + requestID + "'");
                }
                Message request = (Message)mdId2Request[requestID];
                request.Set(Tags.SubscriptionRequestType, SubscriptionRequestType.Disable_previous_Snapshot__plus__Update_Request);
                session.Send(request);
                mdId2Request.Remove(requestID);
            }
        }

        protected void OnInboundApplicationMsg(Object sender, InboundApplicationMsgEventArgs args)
        {
            Message incomingMessage = args.Msg;
            switch (incomingMessage.Type)
            {
                case MsgType.Market_Data_Incremental_Refresh:
                    ProcessMarketDataIncrementalRefresh(incomingMessage);
                    break;
                case MsgType.Market_Data_Snapshot_Full_Refresh:
                    ProcessMarketDataFullRefresh(incomingMessage);
                    break;
                case MsgType.Security_Definition:
                    break;
            }
        }

        private void ProcessMarketDataIncrementalRefresh(Message msg)
        {
            string mdReqID = msg.Get(Tags.MDReqID);

            MarketDataUpdate update;

            lock (lastMD)
            {
                if (!lastMD.TryGetValue(mdReqID, out update))
                {
                    update = new MarketDataUpdate();
                    lastMD.Add(mdReqID, update);
                }
            }

            string symbol = null;

            bool isPriceChanged = false;

            Group mdDEntries = msg.GetGroup(Tags.NoMDEntries);
            for (int i = 0; i < mdDEntries.NumberOfInstances; ++i)
            {
                if (null == symbol)
                {
                    symbol = mdDEntries.Get(Tags.Symbol, i);
                }
                else
                {
                 //   Debug.Assert(mdDEntries.Get(Tags.Symbol, i) == symbol);
                }

                string priceValue = mdDEntries.Get(Tags.MDEntryPx, i);
                if (null != priceValue)
                {
                    double price;
                    if (double.TryParse(priceValue, NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out price))
                    {
                        double size = 0;
                        string sizeValue = mdDEntries.Get(Tags.MDEntrySize, i);
                        if (!string.IsNullOrEmpty(sizeValue))
                            double.TryParse(sizeValue, NumberStyles.Number, CultureInfo.InvariantCulture, out size);
                        string entryType = mdDEntries.Get(Tags.MDEntryType, i);
                        if (MDEntryType.Bid == entryType)
                        {
                            if (update.Bid != price)
                            {
                                update.Bid = price;
                                update.BidSize = size;
                                isPriceChanged = true;
                            }
                        }
                        else if (MDEntryType.Offer == entryType)
                        {
                            if (update.Ask != price)
                            {
                                update.Ask = price;
                                update.AskSize = size;
                                isPriceChanged = true;
                            }
                        }
                    }
                }
            }

            if (isPriceChanged && update.Ask != 0 && update.Bid != 0)
            {
                onMarketData(new MarketTick(symbol, GetMillisecondTimestamp(), (decimal)update.Ask, (decimal)update.Bid, (decimal)(update.Ask + update.Bid) / 2));
            }

            else if (isPriceChanged)
            {
                if (update.Ask == 0)
                {
                    onMarketData(new MarketTick(symbol, GetMillisecondTimestamp(), (decimal)update.Bid, (decimal)update.Bid, (decimal)(update.Bid + update.Bid) / 2));
                }
                else if (update.Bid == 0)
                {
                    onMarketData(new MarketTick(symbol, GetMillisecondTimestamp(), (decimal)update.Ask, (decimal)update.Ask, (decimal)(update.Ask + update.Ask) / 2));
                }
            }
        }

        private void ProcessMarketDataFullRefresh(Message msg)
        {
            //Debug.Assert(MsgType.Market_Data_Snapshot_Full_Refresh == msg.Type);

            MarketDataUpdate update = new MarketDataUpdate();

            string symbol = msg.Get(Tags.Symbol);

            bool isPriceChanged = false;

            Group mdDEntries = msg.GetGroup(Tags.NoMDEntries);
            for (int i = 0; i < mdDEntries.NumberOfInstances; ++i)
            {
                string priceValue = mdDEntries.Get(Tags.MDEntryPx, i);
                if (null != priceValue)
                {
                    double price;
                    if (double.TryParse(priceValue, NumberStyles.Number, CultureInfo.InvariantCulture, out price))
                    {
                        double size = 0;
                        string sizeValue = mdDEntries.Get(Tags.MDEntrySize, i);
                        if (!string.IsNullOrEmpty(sizeValue))
                            double.TryParse(sizeValue, NumberStyles.Number, CultureInfo.InvariantCulture, out size);
                        string entryType = mdDEntries.Get(Tags.MDEntryType, i);
                        if (MDEntryType.Bid == entryType)
                        {
                            if (update.Bid != price || update.BidSize != size)
                            {
                                update.Bid = price;
                                update.BidSize = size;
                                isPriceChanged = true;
                            }
                        }
                        else if (MDEntryType.Offer == entryType)
                        {
                            if (update.Ask != price || update.AskSize != size)
                            {
                                update.Ask = price;
                                update.AskSize = size;
                                isPriceChanged = true;
                            }
                        }
                        else if (MDEntryType.Trade == entryType)
                        {
                            Console.WriteLine("Received Trade Type. Ignore.");
                        }
                    }
                }
            }

            if (isPriceChanged && update.Ask != 0 && update.Bid != 0)
            {
               onMarketData(new MarketTick(symbol, GetMillisecondTimestamp(), (decimal) update.Ask, (decimal)update.Bid, (decimal)(update.Ask + update.Bid) / 2));
            }
        }
    }
}
