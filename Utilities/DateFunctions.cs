﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Utilities
{
    public class DateFunctions
    {
        public static DateTime GetDateTime(long millisecondTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(millisecondTime);
        }

        public static long RoundUpAndConvertToMs(long dt, TimeSpan d)
        {
            return RoundUpAndConvertToMs(GetDateTime(dt), d);
        }

        public static long RoundUpAndConvertToMs(DateTime dt, TimeSpan d)
        {
            return GetInMilliseconds(new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks, dt.Kind));
        }

        public static long GetInMilliseconds(DateTime date)
        {
            DateTime staticDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = date - staticDate;
            return (long)timeSpan.TotalMilliseconds;
        }

        public static int GetTimePeriodInSeconds(int period, string interval)
        {
            switch (interval)
            {
                case "day":
                    return period * 86400; /* There are 86400 seconds in a day */
                case "minute":
                    return period * 60;
                case "hour":
                    return period * 60 * 60;
                case "week":
                    return period * 86400 * 7; 
                case "second":
                    return period;
                default:
                    throw new Exception("Invalid interval");
            }
        }
    }
}