using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using MessageBus;
using MarketDataHandler.Models.Financial;
using Models.Financial;
using MarketDataPublisher;
using System.Collections.Concurrent;

namespace ComputedTicksRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            
            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            catch(Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("ComputedTicksRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("ComputedTicksRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("ComputedTicksRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (true)
            {
                IPEndPoint inEndpoint = RoleEnvironment.Roles["MessagingBusRole"].Instances[0].InstanceEndpoints["MessageBusSubscribe"].IPEndpoint;
                IPEndPoint outEndpoint = RoleEnvironment.Roles["MessagingBusRole"].Instances[0].InstanceEndpoints["MessageBusPublish"].IPEndpoint;

                MessageBusClient<MarketTick> mbClientListen = new MessageBusClient<MarketTick>(inEndpoint.Address.ToString(), inEndpoint.Port, 100000000);
                MessageBusClient<MarketTick> mbClientPublish = new MessageBusClient<MarketTick>(outEndpoint.Address.ToString(), outEndpoint.Port, 100000000);

                ConcurrentDictionary<String, List<Instrument>> instrumentsToCompute = new ConcurrentDictionary<String, List<Instrument>>();

                bool generateInverses = true;

                if (RoleEnvironment.GetConfigurationSettingValue("MarketDataProvider") == "LMAX")
                {
                    foreach (var computedItem in Util.GetMajorLMAXInstruments().Where(x => x.isComputed))
                    {
                        instrumentsToCompute.AddOrUpdate(computedItem.SourceCurrency + "/" + computedItem.ComputeCurrency, new List<Instrument> { computedItem }, (key, oldVal) => { oldVal.Add(computedItem); return oldVal; });
                        instrumentsToCompute.AddOrUpdate(computedItem.ComputeCurrency + "/" + computedItem.DestinationCurrency, new List<Instrument> { computedItem }, (key, oldVal) => { oldVal.Add(computedItem); return oldVal; });
                    }
                }
                else
                {
                    foreach (var computedItem in Util.GetTaggKgiInstruments().Where(x => x.isComputed))
                    {
                        instrumentsToCompute.AddOrUpdate(computedItem.SourceCurrency + "/" + computedItem.ComputeCurrency, new List<Instrument> { computedItem }, (key, oldVal) => { oldVal.Add(computedItem); return oldVal; });
                        instrumentsToCompute.AddOrUpdate(computedItem.ComputeCurrency + "/" + computedItem.DestinationCurrency, new List<Instrument> { computedItem }, (key, oldVal) => { oldVal.Add(computedItem); return oldVal; });
                    }
                }

                QueueExecutor<MarketTick> executionQueue = new QueueExecutor<MarketTick>( x => {
                    /* Get list of computed instruments and publish */
                    List<MarketTick> computedTicks = new List<MarketTick>();
                    
                    if (generateInverses)
                    {
                        computedTicks.Add(new MarketTick(x.InverseSymbol, x.Timestamp, 1/x.AskPrice, 1/x.BidPrice, 1/x.MidPrice));
                    }

                    if (instrumentsToCompute.ContainsKey(x.Symbol))
                    {
                        foreach(var instrument in instrumentsToCompute[x.Symbol])
                        {
                            MarketTick tick = instrument.UpdateFromSubscribed(x.Symbol, x.MidPrice, x.Timestamp);

                            if (tick != null)
                            {
                                computedTicks.Add(tick);
                            }
                        }
                    }
                    
                    Parallel.ForEach(computedTicks, tick => { mbClientPublish.Publish(tick); });
                });

                mbClientPublish.Connect();
                mbClientListen.Connect();

                mbClientListen.Listen(x =>
                {
                    executionQueue.QueueItem(x);
                });

                while (true)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        // Handle Cancellation
                        Console.WriteLine("Cancellation requested. Exiting.");
                        return;
                    }
                }

            }
        }
    }
}
