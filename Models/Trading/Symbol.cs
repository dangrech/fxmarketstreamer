﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Trading
{
    public class Symbol
    {
        public String Name { get; set; }
        public Boolean IsForex { get; set; }
        public Boolean TradesLikeForex { get; set;  }
        public Boolean Tradeable { get; set; }
        public Boolean Shortable { get; set; }
        public Boolean Marketable { get; set; }
    }
}