﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageBus
{
    public static class OnixHelper
    {
        public static String GetLicenseString()
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?> <license version=\"1.0\"> <type>trial</type> <customer> <name>Evaluator</name>" + "</customer> <period> <since>2017-05-24</since> <till>2017-08-23</till> </period> <products> <product id=\"2\" name=\"selfConnect FIX Engine for .NET\" /> </products> <checksum>F0594F2117B48E808ECE061BFE953420</checksum> </license>";
        }

        public static String GetDialectString()
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\" ?> <Dialect xmlns=\"http://ref.onixs.biz/fix/dialects\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://ref.onixs.biz/fix/dialects http://ref.onixs.biz/fix/dialects/dialects-2_14.xsd\"></Dialect>";
        }
    }
}
