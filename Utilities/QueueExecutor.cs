﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessageBus
{
    public class QueueExecutor<T>
    {
        Queue<T> Queue;
        Action<T> executor;
        TaskFactory tf;

        public QueueExecutor(Action<T> executor)
        {
            Queue = new Queue<T>();
            tf = new TaskFactory();
            this.executor = executor;
            tf.StartNew(Run);
        }        

        public void QueueItem(T item)
        {
            lock (Queue)
            {
                Queue.Enqueue(item);
            }
        }

        private void Run()
        {
            while (true)
            {
                if (Queue.Count > 0)
                {
                    T item; 

                    lock (Queue)
                    {
                        item = Queue.Dequeue();
                    }

                    tf.StartNew(() => executor(item));
                }
            }
        }



    }
}
