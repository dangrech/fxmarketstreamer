﻿using MarketDataHandler.Models.Financial;
using Microsoft.WindowsAzure.ServiceRuntime;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketDataPublisher
{
    public class RedisTickDataDumper : ITickDataDumper
    {
        ConnectionMultiplexer Redis; 

        public RedisTickDataDumper()
        {
            String url =  RoleEnvironment.GetConfigurationSettingValue("RedisConnection");
            Redis = ConnectionMultiplexer.Connect(url);
        }

        public async Task DumpTick(MarketTick tick)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{\"ask\":");
            sb.Append(tick.AskPrice.ToString("0.######"));
            sb.Append(", \"bid\":");
            sb.Append(tick.BidPrice.ToString("0.######"));
            sb.Append("}");

            await Redis.GetDatabase().StringSetAsync(tick.Symbol.Replace('/', '_'), sb.ToString(), flags: CommandFlags.FireAndForget);
        }

        public async Task DumpTickInverse(MarketTick tick)
        {
            await Redis.GetDatabase().StringSetAsync(tick.InverseSymbol.Replace('/', '_'), tick.InverseMidPrice.ToString("0.######"), flags : CommandFlags.FireAndForget);
        }

        public decimal GetInstrumentValue(String instrument)
        {
            String value = Redis.GetDatabase().StringGet(instrument.Replace('/', '_'));

            try
            {
                return decimal.Parse(value);
            }
            catch (Exception)
            {
                return 0.0m;
            }
        }

        public void DumpSnapshot(Dictionary<string, decimal> lastInstrumentValues)
        {
            foreach (var keyValue in lastInstrumentValues)
            {
                if (keyValue.Value != 0)
                {
                    Redis.GetDatabase().StringSet(keyValue.Key, keyValue.Value.ToString("0.######"), flags: CommandFlags.FireAndForget);
                }
            }
        }
    }
}
