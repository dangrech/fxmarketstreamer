﻿using MarketDataHandler.Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Financial
{
    public class Instrument
    {
        public String Symbol { get; set; }
        public String InverseSymbol { get; set; }
        public String SourceCurrency { get; set; }
        public String DestinationCurrency {get ;set; }
        public decimal LastPrice { get; set;  }
        public bool isNDF { get; set; }
        public bool isComputed { get; set;  }
        public String ComputeCurrency { get; set; }
        public List<String> SubscribedTo { get; set;  }
        private Dictionary<String, decimal> lastSeenSubscribedPrices { get; set;  }
        
        public String NDFPeriod { get; set;  }

        public Instrument(String Symbol, bool isNDF = false, bool isComputed = false, String computeCurrency = null, String ndfPeriod = null)
        {
            this.Symbol = Symbol;
            this.SourceCurrency = Symbol.Split('/')[0];
            this.DestinationCurrency = Symbol.Split('/')[1];
            this.InverseSymbol = this.DestinationCurrency + '/' + this.SourceCurrency;
            this.isNDF = isNDF;
            this.NDFPeriod = ndfPeriod;
            this.isComputed = isComputed;

            if (isComputed)
            {
                ComputeCurrency = computeCurrency;
                SubscribedTo = new List<string>();
                SubscribedTo.Add(SourceCurrency + "/" + ComputeCurrency);
                SubscribedTo.Add(ComputeCurrency + "/" + DestinationCurrency);

                lastSeenSubscribedPrices = new Dictionary<string, decimal>();
                lastSeenSubscribedPrices.Add(SourceCurrency + "/" + ComputeCurrency, 0m);
                lastSeenSubscribedPrices.Add(ComputeCurrency + "/" + DestinationCurrency,0m);

            }
        }

        public bool IsSubscribedTo(String currency)
        {
            return SubscribedTo.Contains(currency);
        }

        public MarketTick UpdateFromSubscribed(String instrument, decimal newValue, long timestamp)
        {
            lastSeenSubscribedPrices[instrument] = newValue;

            decimal a = lastSeenSubscribedPrices[SourceCurrency + "/" + ComputeCurrency];
            decimal b = lastSeenSubscribedPrices[ComputeCurrency + "/" + DestinationCurrency];

            if (a == 0 || b == 0)
            {
                return null;
            }

            LastPrice = a * b;
            return new MarketTick(Symbol, timestamp, LastPrice, LastPrice, LastPrice, true);
        }
        
        
    }
}