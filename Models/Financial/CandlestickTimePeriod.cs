﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Financial
{
    public enum CandlestickPeriod
    {
        TENSECONDS = 10,
        ONEMINUTE = 60,
        TENMINUTES = 600
    }
}