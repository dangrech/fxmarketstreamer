﻿using Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataIngestionRole
{
    public class Util
    {
        public static String GenerateMarketSnapshot(String sourceSymbol, List<String> symbolsToSubscribeTo, Dictionary<string, decimal> lastPrices)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"");
            sb.Append(sourceSymbol.ToLower());
            sb.Append("\":{");

            bool wrote = false; 

            for (int i = 0; i < symbolsToSubscribeTo.Count; i++)
            {
                if (!lastPrices.ContainsKey(symbolsToSubscribeTo.ElementAt(i)) || lastPrices[symbolsToSubscribeTo.ElementAt(i)] == 0 )
                {
                    continue;
                }

                else if (i != 0 && wrote)
                {
                    sb.Append(",");
                }

                sb.Append("\"");
                sb.Append(symbolsToSubscribeTo.ElementAt(i).Split('/')[1].ToLower());
                sb.Append("\":");
                sb.Append(lastPrices[symbolsToSubscribeTo.ElementAt(i)].ToString("0.######"));
                wrote = true;
            }

            sb.Append("}");
            sb.Append("}");

            return sb.ToString();
        }

        public static List<Instrument> GetTaggKgiInstruments()
        {
            return new List<Instrument>
            {
                /* Base Currencies */
                 new Instrument("USD/CNY", true, false, null, "1M"),
                new Instrument("USD/INR", true, false, null, "1W"),
                new Instrument("USD/IDR", true, false, null, "1W"),
                new Instrument("EUR/USD"),
                new Instrument("USD/CAD"),
                new Instrument("USD/JPY"),
                new Instrument("USD/HKD"),
                new Instrument("USD/CHF"),
                new Instrument("USD/SGD"),
                new Instrument("GBP/USD"),
                new Instrument("AUD/USD"),
                new Instrument("NZD/USD"),

                /*Computed Currencies */

                new Instrument("EUR/CAD", false, true, "USD"),
                new Instrument("EUR/JPY", false, true, "USD"),
                new Instrument("EUR/CNY", false, true, "USD"),
                new Instrument("EUR/HKD", false, true, "USD"),
                new Instrument("EUR/CHF", false, true, "USD"),
                new Instrument("EUR/INR", false, true, "USD"),
                new Instrument("EUR/IDR", false, true, "USD"),
                new Instrument("EUR/SGD", false, true, "USD"),
                new Instrument("EUR/GBP", false, true, "USD"),
                new Instrument("EUR/AUD", false, true, "USD"),
                new Instrument("EUR/NZD", false, true, "USD"),

                new Instrument("CAD/JPY", false, true, "USD"),
                new Instrument("CAD/CNY", false, true, "USD"),
                new Instrument("CAD/HKD", false, true, "USD"),
                new Instrument("CAD/CHF", false, true, "USD"),
                new Instrument("CAD/INR", false, true, "USD"),
                new Instrument("CAD/IDR", false, true, "USD"),
                new Instrument("CAD/SGD", false, true, "USD"),
                new Instrument("CAD/GBP", false, true, "USD"),
                new Instrument("CAD/AUD", false, true, "USD"),
                new Instrument("CAD/NZD", false, true, "USD"),

                new Instrument("JPY/CNY", false, true, "USD"),
                new Instrument("JPY/HKD", false, true, "USD"),
                new Instrument("JPY/CHF", false, true, "USD"),
                new Instrument("JPY/INR", false, true, "USD"),
                new Instrument("JPY/IDR", false, true, "USD"),
                new Instrument("JPY/SGD", false, true, "USD"),
                new Instrument("JPY/GBP", false, true, "USD"),
                new Instrument("JPY/AUD", false, true, "USD"),
                new Instrument("JPY/NZD", false, true, "USD"),

                new Instrument("CNY/HKD", false, true, "USD"),
                new Instrument("CNY/CHF", false, true, "USD"),
                new Instrument("CNY/INR", false, true, "USD"),
                new Instrument("CNY/IDR", false, true, "USD"),
                new Instrument("CNY/SGD", false, true, "USD"),
                new Instrument("CNY/GBP", false, true, "USD"),
                new Instrument("CNY/AUD", false, true, "USD"),
                new Instrument("CNY/NZD", false, true, "USD"),

                new Instrument("HKD/CHF", false, true, "USD"),
                new Instrument("HKD/INR", false, true, "USD"),
                new Instrument("HKD/IDR", false, true, "USD"),
                new Instrument("HKD/SGD", false, true, "USD"),
                new Instrument("HKD/GBP", false, true, "USD"),
                new Instrument("HKD/AUD", false, true, "USD"),
                new Instrument("HKD/NZD", false, true, "USD"),

                new Instrument("CHF/INR", false, true, "USD"),
                new Instrument("CHF/IDR", false, true, "USD"),
                new Instrument("CHF/SGD", false, true, "USD"),
                new Instrument("CHF/GBP", false, true, "USD"),
                new Instrument("CHF/AUD", false, true, "USD"),
                new Instrument("CHF/NZD", false, true, "USD"),

                new Instrument("INR/IDR", false, true, "USD"),
                new Instrument("INR/SGD", false, true, "USD"),
                new Instrument("INR/GBP", false, true, "USD"),
                new Instrument("INR/AUD", false, true, "USD"),
                new Instrument("INR/NZD", false, true, "USD"),

                new Instrument("IDR/SGD", false, true, "USD"),
                new Instrument("IDR/GBP", false, true, "USD"),
                new Instrument("IDR/AUD", false, true, "USD"),
                new Instrument("IDR/NZD", false, true, "USD"),

                new Instrument("SGD/GBP", false, true, "USD"),
                new Instrument("SGD/AUD", false, true, "USD"),
                new Instrument("SGD/NZD", false, true, "USD"),

                new Instrument("GBP/AUD", false, true, "USD"),
                new Instrument("GBP/NZD", false, true, "USD"),

                new Instrument("AUD/NZD", false, true, "USD")
            };
        }

        public static List<Instrument> GetAllKGIInstruments()
        {
            return new List<Instrument>
            {
                new Instrument("AUD/CAD"),
                new Instrument("AUD/CHF"),
                new Instrument("AUD/JPY"),
                new Instrument("AUD/NZD"),
                new Instrument("AUD/SGD"),
                new Instrument("AUD/USD"),
                new Instrument("CAD/CHF"),
                new Instrument("CAD/JPY"),
                new Instrument("CHF/JPY"),
                new Instrument("EUR/AUD"),
                new Instrument("EUR/CAD"),
                new Instrument("EUR/CHF"),
                new Instrument("EUR/GBP"),
                new Instrument("EUR/JPY"),
                new Instrument("EUR/NZD"),
                new Instrument("EUR/SGD"),
                new Instrument("EUR/USD"),
                new Instrument("GBP/AUD"),
                new Instrument("GBP /CAD"),
                new Instrument("GBP/CHF"),
                new Instrument("GBP/JPY"),
                new Instrument("GBP/NZD"),
                new Instrument("GBP/SGD"),
                new Instrument("GBP/USD"),
                new Instrument("NZD/JPY"),
                new Instrument("NZD/USD"),                         
                new Instrument("USD/SGD"),
                new Instrument("USD/THB"),
                new Instrument("USD/TWD"),
                new Instrument("XAG/USD"),
                new Instrument("USD/CAD"),
                new Instrument("USD/CHF"),
                new Instrument("USD/CNH"),
                new Instrument("USD/HKD"),
                new Instrument("USD/JPY"),
                new Instrument("USD/KRW", true),
                new Instrument("USD/PHP", true),
                new Instrument("USD/BRL", true),
                new Instrument("USD/IDR", true),
                new Instrument("USD/INR", true),
                new Instrument("USD/CNY", true),
            };       
        }

        // EUR, USD, GBP, YEN , SGD, CHF, AUD, NZD
        public static List<Instrument> GetMajorLMAXInstruments()
        {
            return new List<Instrument>
            {
                /* EUR */
                new Instrument("EUR/USD"),
                new Instrument("EUR/GBP"),
                new Instrument("EUR/JPY"),
                new Instrument("EUR/SGD"),
                new Instrument("EUR/CHF"),
                new Instrument("EUR/AUD"),
                new Instrument("EUR/NZD"),

                /* USD */
                new Instrument("GBP/USD"),
                new Instrument("USD/JPY"),
                new Instrument("USD/SGD"),
                new Instrument("USD/CHF"),
                new Instrument("AUD/USD"),
                new Instrument("NZD/USD"),

                /* GBP */
                new Instrument("GBP/AUD"),
                new Instrument("GBP/CHF"),
                new Instrument("GBP/JPY"),
                new Instrument("GBP/NZD"),
                new Instrument("GBP/SGD"),

                /* YEN */
                new Instrument("AUD/JPY"),
                new Instrument("CHF/JPY"),
                new Instrument("NZD/JPY"),
                new Instrument("SGD/JPY", false, true, "USD"),

                /*  SGD */
                new Instrument( "NZD/SGD"),
                new Instrument( "CHF/SGD", false, true, "USD"),
                new Instrument( "AUD/SGD", false, true, "USD"),

                /* CHF */
                new Instrument("AUD/CHF"),
                new Instrument( "NZD/CHF"),

                /* AUD */ 
                new Instrument("AUD/NZD")
            };
        }

        public static List<Instrument> GetAllLMAXInstruments()
        {
            return new List<Instrument> {
            new Instrument("AUD/CAD"),
            new Instrument("AUD/CHF"),
            new Instrument("AUD/JPY"),
            new Instrument("AUD/NZD"),
            new Instrument("AUD/USD"),
            new Instrument("CAD/CHF"),
            new Instrument("CAD/JPY"),
            new Instrument("CHF/JPY"),
            new Instrument("EUR/AUD"),
            new Instrument("EUR/CAD"),
            new Instrument("EUR/CHF"),
            new Instrument("EUR/CZK"),
            new Instrument("EUR/DKK"),
            new Instrument("EUR/GBP"),
            new Instrument("EUR/HKD"),
            new Instrument("EUR/HUF"),
            new Instrument("EUR/JPY"),
            new Instrument("EUR/MXN"),
            new Instrument("EUR/NOK"),
            new Instrument("EUR/NZD"),
            new Instrument("EUR/PLN"),
            new Instrument("EUR/RON"),
            new Instrument("EUR/RUB"),
            new Instrument("EUR/SEK"),
            new Instrument( "EUR/SGD"),
            new Instrument("EUR/TRY"),
            new Instrument("EUR/USD"),
            new Instrument("EUR/ZAR"),
            new Instrument("GBP/AUD"),
            new Instrument("GBP/CAD"),
            new Instrument("GBP/CHF"),
            new Instrument("GBP/CZK"),
            new Instrument("GBP/DKK"),
            new Instrument("GBP/HKD"),
            new Instrument("GBP/HUF"),
           new Instrument( "GBP/JPY"),
           new Instrument( "GBP/MXN"),
            new Instrument("GBP/NOK"),
            new Instrument("GBP/NZD"),
           new Instrument( "GBP/PLN"),
           new Instrument( "GBP/SEK"),
           new Instrument( "GBP/SGD"),
           new Instrument( "GBP/TRY"),
           new Instrument( "GBP/USD"),
           new Instrument( "GBP/ZAR"),
           new Instrument( "NOK/SEK"),
           new Instrument( "NZD/CAD"),
           new Instrument( "NZD/CHF"),
           new Instrument( "NZD/JPY"),
           new Instrument( "NZD/SGD"),
           new Instrument( "NZD/USD"),
           new Instrument( "USD/CAD"),
           new Instrument( "USD/CHF"),
           new Instrument( "USD/CNH"),
           new Instrument( "USD/CZK"),
           new Instrument( "USD/DKK"),
           new Instrument( "USD/HKD"),
           new Instrument( "USD/HUF"),
           new Instrument( "USD/ILS"),
           new Instrument( "USD/JPY"),
           new Instrument( "USD/MXN"),
           new Instrument( "USD/NOK"),
           new Instrument( "USD/PLN"),
           new Instrument( "USD/RON"),
           new Instrument( "USD/RUB"),
           new Instrument( "USD/SEK"),
           new Instrument( "USD/SGD"),
           new Instrument( "USD/TRY"),
           new Instrument( "USD/ZAR"),
            };
        }
    }
}
