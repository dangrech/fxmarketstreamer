using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using MessageBus;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Threading;
using Services;
using MarketDataHandler.Models.Financial;
using Models.Financial;
using System.Net;
using System.Diagnostics;

namespace DataIngestionRole
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart()
        {
            return base.OnStart();
        }

        public override void Run()
        {
            while (true)
            {
                try
                {
                    IPEndPoint endpoint = RoleEnvironment.Roles["MessageBusRole"].Instances[0].InstanceEndpoints["MessageBusPublish"].IPEndpoint;

                    MessageBusClient<MarketTick> mbClient = new MessageBusClient<MarketTick>(endpoint.Address.ToString(), endpoint.Port, 100000000);

                    mbClient.Connect();

                    List<Instrument> symbolList = Util.GetMajorLMAXInstruments();

                    AbstractMarketStreamingService dataStreamer = new LmaxMarketStreamingService(x => { mbClient.Publish(x); },
                        symbolList.Where(x => !x.isComputed).ToList(), newState =>
                        {
                            if (newState == ConnectionState.CONNECTION_OPEN)
                            {
                                Trace.TraceInformation("Lmax Connection Open");
                            }
                            else
                            {
                                Trace.TraceInformation("Lmax Connection Closed");
                            }
                        });

                    dataStreamer.StartSession();

                    while (true)
                    {
                        Thread.Sleep(5000);
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(30000);
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
