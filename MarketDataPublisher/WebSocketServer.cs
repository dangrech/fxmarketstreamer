﻿using Fleck;
using MarketDataHandler.Models.Financial;
using Models.Financial;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketDataPublisher
{
    public class WebSocketServer<T> where T : IPublishable, ISubscribable
    {
        ConcurrentDictionary<Guid, IWebSocketConnection> sockets;
        ConcurrentDictionary<String, List<Guid>> subscriptions;
        
        public WebSocketServer()
        {
            sockets = new ConcurrentDictionary<Guid, IWebSocketConnection>();
            subscriptions = new ConcurrentDictionary<String,List<Guid>>();
        }

        public Task SendToSingle(String message, IWebSocketConnection socket)
        {
            return socket.Send(message);
        }

        public Task SendToAll(String message)
        {
            return Task.Run(() =>
            {
                Parallel.ForEach(sockets, x =>
                {
                    x.Value.Send(message);
                });
            });
        }

        public Task AddSocket(IWebSocketConnection newSocket)
        {
            return Task.Run(() => sockets.AddOrUpdate(newSocket.ConnectionInfo.Id, newSocket, (key,existingVal) => { return newSocket; }));
        }

        public Task RemoveSocket(IWebSocketConnection oldSocket)
        {
            IWebSocketConnection retrieved;
            return Task.WhenAll(UnsubscribeFromAll(oldSocket), Task.Run(() => sockets.TryRemove(oldSocket.ConnectionInfo.Id, out retrieved)));
        }

        public Task Subscribe(String subscriptionKey, IWebSocketConnection socket)
        {
            return Task.Run(() =>
           {
               subscriptions.AddOrUpdate(subscriptionKey, x => { return new List<Guid> { socket.ConnectionInfo.Id }; },
                   (key, existing) =>
                   {

                       if (!existing.Contains(socket.ConnectionInfo.Id))
                       {
                               existing.Add(socket.ConnectionInfo.Id);
                       }

                       return existing;
                   });
           });
        }

        public Task Unsubscribe(String subscriptionKey, IWebSocketConnection socket)
        {
            return Task.Run(() =>
           {
               if (subscriptions.ContainsKey(subscriptionKey))
               {
                   subscriptions[subscriptionKey].Remove(socket.ConnectionInfo.Id);
               }
           });
        }

        public Task UnsubscribeFromAll(IWebSocketConnection socket)
        {
            return Task.Run(() =>
            {
                var subscribed = subscriptions.Where(x => x.Value.Contains(socket.ConnectionInfo.Id)).ToList();

                foreach (var subscription in subscribed)
                {

                    subscription.Value.Remove(socket.ConnectionInfo.Id);
                    
                }
            });
        }

        public Task Update(T entity)
        {
            return Push(entity);
        }

        private Task Push(T entity)
        {
           return Task.Run(() =>
           {
               ICollection<String> subscribeKeys = entity.GetSubscribeKeys();

               var subscribedSockets = subscriptions.Where(x => subscribeKeys.Contains(x.Key)).Distinct().ToList();

               Parallel.ForEach(subscribedSockets, x =>
               {
                   var subscriptionsKey = x.Value;

                   Parallel.ForEach(subscriptionsKey, socketId =>
                   {
                       try
                       {
                           IWebSocketConnection socket;
                           sockets.TryGetValue(socketId, out socket);
                           socket.Send(entity.GetPublishableString());
                       }
                       catch(Exception ex){}
                   });

               });
           });

        }
    }
}
