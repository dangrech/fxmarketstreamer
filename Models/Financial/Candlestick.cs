﻿using MarketDataHandler.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Financial
{
    public class Candlestick
    {
        public String Symbol { get; set;  }
        public DateTime Date { get; set; }
        public decimal Open { get; set; }
        public decimal Close { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Volume { get; set; }
        public decimal Adj_Close { get; set; }

        private long timestampStart;
        private long timestampEnd;

        public Candlestick Update(decimal price)
        {
            if (Open == 0)
            {
                Open = price;
                Close = price;
                High = price;
                Low = price;
            }

            Close = price;
            Low = Math.Min(Low, price);
            High = Math.Max(High, price);
            return this;
        }

        public Candlestick(String symbol, long timestampStart, long timestampEnd, decimal openPrice)
        {
            this.Symbol = symbol;
            this.timestampStart = timestampStart;
            this.timestampEnd = timestampEnd;
            this.Date = DateFunctions.GetDateTime(timestampStart);
            this.Open = openPrice;
            this.Volume = 0;
            this.Adj_Close = 0;
        }

        public Candlestick(String symbol, long timestampStart, long timestampEnd, decimal open, decimal high, decimal low, decimal close)
        {
            this.Symbol = symbol;
            this.timestampStart = timestampStart;
            this.timestampEnd = timestampEnd;
            this.Date = DateFunctions.GetDateTime(timestampStart);
            this.Open = open;
            this.Close = close;
            this.Low = low;
            this.High = high;
            this.Volume = 0;
            this.Adj_Close = close;
        }

        public long GetTimestampStart()
        {
            return timestampStart;
        }

        public long GetTimestampEnd()
        {
            return timestampEnd;
        }
    }
}