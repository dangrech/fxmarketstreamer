﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace MessageBus
{
    public class TCPMessageBus<T>
    {
        QueueExecutor<Message<T>> ObjectQueue;
        QueueExecutor<Message<T>> InboundTempQueue;
        TcpListener IncomingListener;
        TcpListener OutgoingListener;
        ICollection<TcpClient> IncomingClients;
        ICollection<TcpClient> OutgoingClients;
        ICollection<NetworkStream> OutgoingStreams;
        
        public TCPMessageBus(IPAddress incomingIp, int incomingPort, int outgoingPort)
        {
            this.ObjectQueue = new QueueExecutor<Message<T>>(Push);
            this.InboundTempQueue = new QueueExecutor<Message<T>>(ParseAndPush);

            IncomingListener = new TcpListener(incomingIp,incomingPort);
            OutgoingListener = new TcpListener(incomingIp, outgoingPort);
            IncomingClients = new List<TcpClient>();
            OutgoingClients = new List<TcpClient>();
            OutgoingStreams = new List<NetworkStream>();
        }

        public Task Start()
        {
            return Task.WhenAll(Task.Run(() => StartIncoming()), Task.Run(() => StartOutgoing()));
        }

        private void StartIncoming()
        {
            IncomingListener.Start();

            while (true)
            {
                TcpClient incomingClient = IncomingListener.AcceptTcpClient();
                IncomingClients.Add(incomingClient);
                Task.Run(() => ListenForIncomingData(incomingClient));
            }
        }

        private void ListenForIncomingData(TcpClient incomingClient)
        {
            Byte[] incomingBytes = new Byte[250000000];

            try
            {

                while (true)
                {
                    NetworkStream incomingStream = incomingClient.GetStream();
                    int writeIndex = 0;
                    int readIndex = 0;

                    bool inObject = false;
                    uint currentObjectLength = 0;

                    while ((writeIndex += incomingStream.Read(incomingBytes, writeIndex, incomingBytes.Length - writeIndex)) != 0)
                    {
                        if (!inObject && (writeIndex - readIndex) >= 4)
                        {
                            currentObjectLength = BitConverter.ToUInt32(incomingBytes, readIndex);
                            readIndex += 4;
                            inObject = true;
                        }

                        if (inObject && (writeIndex - readIndex) >= currentObjectLength)
                        {
                            Byte[] data = new Byte[currentObjectLength];
                            Array.Copy(incomingBytes, readIndex, data, 0, currentObjectLength);
                            readIndex += (int)currentObjectLength;

                            Message<T> message = new Message<T>(currentObjectLength, data);
                            InboundTempQueue.QueueItem(message);
                            inObject = false;
                        }

                        if (writeIndex == incomingBytes.Length)
                        {
                            // Got to end of array, to avoid problems compy current temp to beginning of array and start from there again
                            Array.Copy(incomingBytes, readIndex, incomingBytes, 0, writeIndex - readIndex);
                            writeIndex = writeIndex - readIndex;
                            readIndex = 0;
                            inObject = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                incomingClient.Close();
                IncomingClients.Remove(incomingClient);
                
            }
        }

        private void StartOutgoing()
        {
            OutgoingListener.Start();

                while (true)
                {
                    TcpClient outgoingClient = OutgoingListener.AcceptTcpClient();

                lock (OutgoingClients)
                    {
                        OutgoingClients.Add(outgoingClient);
                        OutgoingStreams.Add(outgoingClient.GetStream());
                    }
                }
        }

        private void ParseAndPush(Message<T> entity)
        {
            ObjectQueue.QueueItem(entity);
        }

        private void Push(Message<T> item)
        {
            Byte[] data = item.GetBytes();

            Parallel.ForEach(OutgoingStreams, x => {

                try
                {
                    x.Write(data, 0, data.Length);
                }
                catch(Exception ex)
                {
                    Trace.TraceError(ex.Message);
                }
            });   
        }
    }
}
