﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketDataHandler.Models.Financial;
using Microsoft.WindowsAzure.ServiceRuntime;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace MarketDataPublisher
{
    public class KafkaTickDataDumper : ITickDataDumper
    {
        String brokerList;
        Dictionary<string, object> kafkaConfig; 
        Producer<Null, string> kafkaProducer; 
        
        public KafkaTickDataDumper()
        {
            brokerList = RoleEnvironment.GetConfigurationSettingValue("KafkaBrokerList");
            kafkaConfig = new Dictionary<string, object> { { "bootstrap.servers", brokerList } };
            kafkaProducer = new Producer<Null, string>(kafkaConfig, null, new StringSerializer(Encoding.UTF8)) ;
        }
        public void DumpSnapshot(Dictionary<string, decimal> lastInstrumentValues)
        {
            // Do Nothing
        }

        public Task DumpTick(MarketTick tick)
        {
            return kafkaProducer.ProduceAsync(tick.Symbol.Split('/')[0], null, ConvertTickToKafkaFormat(tick));
           
        }

        public Task DumpTickInverse(MarketTick tick)
        {
           return kafkaProducer.ProduceAsync(tick.InverseSymbol.Split('/')[0], null, ConvertTickToKafkaFormat(tick, true));
        }

        private String ConvertTickToKafkaFormat(MarketTick tick, bool useInverse = false)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"ask\":\"");

            if (!useInverse)
                sb.Append(tick.AskPrice);
            else
                sb.Append(tick.InverseAskPrice);

            sb.Append("\",\"instrument\":\"");

            if (!useInverse)
                sb.Append(tick.Symbol);
            else
                sb.Append(tick.InverseSymbol);

            sb.Append("\",\"time\":");
            sb.Append(tick.Timestamp);
            sb.Append(",\"bid\":\"");

            if (!useInverse)
                sb.Append(tick.BidPrice);
            else
                sb.Append(tick.InverseBidPrice);

            sb.Append("\"}");

            return sb.ToString();
        }
    }
}
