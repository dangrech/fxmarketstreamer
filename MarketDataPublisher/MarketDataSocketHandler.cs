﻿using Fleck;
using MarketDataHandler.Models.Financial;
using Models.Financial;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MessageBus;

namespace MarketDataPublisher
{
    public class MarketDataSocketHandler
    {
        /* Stores all sockets */
        Dictionary<Guid, IWebSocketConnection> allSockets = new Dictionary<Guid, IWebSocketConnection>();

        /* Stores sockets ids which will receive updates for a given instrument */
        Dictionary<String, List<Guid>> socketSubscriptions = new Dictionary<string, List<Guid>>();

        /* Stores last received midprice for each instrument */
        public Dictionary<String, decimal> lastInstrumentValues { get; }

        /* Computed Instruments */
        Dictionary<String, List<Instrument>> baseToComputedInstruments;


        public MarketDataSocketHandler(List<Instrument> instrumentList)
        {
            lastInstrumentValues = new Dictionary<string, decimal>();

            baseToComputedInstruments = new Dictionary<string, List<Instrument>>();

            instrumentList.ForEach(x =>
            {
                lastInstrumentValues.Add(x.Symbol, 0.0m);
                lastInstrumentValues.Add(x.InverseSymbol, 0.0m);
            });

            instrumentList.Where(x => x.isComputed).ToList().ForEach(x =>
            {
                x.SubscribedTo.ForEach(y =>
                {

                    if (!baseToComputedInstruments.ContainsKey(y))
                    {
                        baseToComputedInstruments[y] = new List<Instrument>();
                    }

                    baseToComputedInstruments[y].Add(x);
                });
            });
        }

        public void LoadDataFromRedis(RedisTickDataDumper redisConnection)
        {
            List<String> keys = new List<String>();

            foreach (var instrument in lastInstrumentValues.Keys)
            {
                keys.Add(instrument);
            }

            foreach (var instrument in keys)
            {
                lastInstrumentValues[instrument] = redisConnection.GetInstrumentValue(instrument);
            }
        }

        public Task UnsubscribeFromAll(IWebSocketConnection socket)
        {
            return Task.Run(() => {
                lock (socketSubscriptions)
                {
                    foreach (var entry in socketSubscriptions)
                    {
                        if (entry.Value.Contains(socket.ConnectionInfo.Id))
                        {
                            entry.Value.Remove(socket.ConnectionInfo.Id);
                        }
                    }
                }
            });
        }

        public Task UnsubscribeSocketFromInstrument(IWebSocketConnection socket, String instrument)
        {
            return Task.Run(() =>
            {
                lock (socketSubscriptions)
                {
                    if (socketSubscriptions.ContainsKey(instrument))
                    {
                        socketSubscriptions[instrument].Remove(socket.ConnectionInfo.Id);
                    }
                }
            });
        }

        public Task SubscribeSocketToInstrument(IWebSocketConnection socket, String instrument)
        {
           return Task.Run(() =>
           {
               lock (socketSubscriptions)
               {
                   if (socketSubscriptions.ContainsKey(instrument))
                   {
                       if (!socketSubscriptions[instrument].Contains(socket.ConnectionInfo.Id))
                       {
                           socketSubscriptions[instrument].Add(socket.ConnectionInfo.Id);
                       }
                   }
                   else
                   {
                       socketSubscriptions[instrument] = new List<Guid>() { socket.ConnectionInfo.Id };
                   }
               }
           });
        }

        public Task AddNewSocket(IWebSocketConnection newSocket)
        {
            return Task.Run(() => {
                lock (allSockets)
                {
                    allSockets.Add(newSocket.ConnectionInfo.Id, newSocket);
                }
            });
        }

        public Task RemoveSocket(IWebSocketConnection oldSocket)
        {
           return Task.Run(() =>
           {
               lock (allSockets)
               {
                   if (allSockets.ContainsKey(oldSocket.ConnectionInfo.Id))
                   {
                       allSockets.Remove(oldSocket.ConnectionInfo.Id);
                   }
               }
           });
        }

        public Task SendMarketClosed()
        {
            return Task.Run(() => {
                lock (allSockets)
                {
                    Task closed = Task.Run(() =>
                    {
                        Parallel.ForEach(allSockets, x => { x.Value.Send(JsonConvert.SerializeObject(MarketDataResponseError.BrokerNotAvailable())); });
                    });
                }
            });
        }

        public List<MarketTick> PushMarketDataToSubscribedSockets(MarketTick marketTick)
        {
            List<MarketTick> computedTicks = new List<MarketTick>();
            Task socketTask = Task.FromResult<Object>(null);
            Task inverseTask = Task.FromResult<Object>(null);
            Task updateComputedTask = Task.FromResult<Object>(null);
            Task updateInverseComputedTask = Task.FromResult<Object>(null);

            Dictionary<String, List<Guid>> socketSubscriptionsCopy;
            Dictionary<Guid, IWebSocketConnection> allSocketsCopy;

            lock (this.socketSubscriptions)
            {
                socketSubscriptionsCopy = this.socketSubscriptions.ToDictionary(x => x.Key, x => x.Value.Select(y => new Guid(y.ToString())).ToList());
            }

            lock (this.allSockets)
            {
                allSocketsCopy = this.allSockets.ToDictionary(x => x.Key, x => x.Value);
            }

                    if (socketSubscriptionsCopy.ContainsKey(marketTick.Symbol))
                    {
                        socketTask = Task.Run(() => { socketSubscriptionsCopy[marketTick.Symbol].ForEach(x => { allSocketsCopy[x].Send(marketTick.ToTaggJsonString()); }); });
                    }

                    if (socketSubscriptionsCopy.ContainsKey(marketTick.InverseSymbol))
                    {
                        inverseTask = Task.Run(() =>
                        socketSubscriptionsCopy[marketTick.InverseSymbol].ForEach(x => { allSocketsCopy[x].Send(marketTick.InverseToTaggJsonString()); }));
                    }

                    lock (lastInstrumentValues)
                    {
                        lastInstrumentValues[marketTick.Symbol] = marketTick.MidPrice;
                        lastInstrumentValues[marketTick.InverseSymbol] = marketTick.InverseMidPrice;
                    }

                    if (baseToComputedInstruments.ContainsKey(marketTick.Symbol))
                    {
                        updateComputedTask = Task.Run(() =>

                        baseToComputedInstruments[marketTick.Symbol].ForEach(x =>
                        {
                            MarketTick tick = x.UpdateFromSubscribed(marketTick.Symbol, lastInstrumentValues[marketTick.Symbol], marketTick.Timestamp);

                            computedTicks.Add(tick);

                            lastInstrumentValues[tick.Symbol] = tick.MidPrice;
                            lastInstrumentValues[tick.InverseSymbol] = tick.InverseMidPrice;

                            if (socketSubscriptionsCopy.ContainsKey(tick.Symbol))
                            {
                                socketSubscriptionsCopy[tick.Symbol].ForEach(y =>
                                {
                                    allSocketsCopy[y].Send(tick.ToTaggJsonString());
                                });
                            }

                            if (socketSubscriptionsCopy.ContainsKey(tick.InverseSymbol))
                            {
                                socketSubscriptionsCopy[tick.InverseSymbol].ForEach(y =>
                                {
                                    allSocketsCopy[y].Send(tick.InverseToTaggJsonString());
                                });
                            }

                        }));
                    }

                    if (baseToComputedInstruments.ContainsKey(marketTick.InverseSymbol))
                    {
                        updateInverseComputedTask = Task.Run(() =>

                        baseToComputedInstruments[marketTick.InverseSymbol].ForEach(x =>
                        {

                            MarketTick tick = x.UpdateFromSubscribed(marketTick.InverseSymbol, lastInstrumentValues[marketTick.InverseSymbol], marketTick.Timestamp);

                            computedTicks.Add(tick);

                            lock (lastInstrumentValues)
                            {
                                lastInstrumentValues[tick.Symbol] = tick.MidPrice;
                                lastInstrumentValues[tick.InverseSymbol] = tick.InverseMidPrice;
                            }

                            if (socketSubscriptionsCopy.ContainsKey(tick.Symbol))
                            {
                                socketSubscriptionsCopy[tick.Symbol].ForEach(y => { allSocketsCopy[y].Send(tick.ToTaggJsonString()); });

                            }

                            if (socketSubscriptionsCopy.ContainsKey(tick.InverseSymbol))
                            {
                                socketSubscriptionsCopy[tick.InverseSymbol].ForEach(y => { allSocketsCopy[y].Send(tick.InverseToTaggJsonString()); });
                            }
                        }));
                    }

                    updateComputedTask.Wait();
                    updateInverseComputedTask.Wait();
                    updateComputedTask.Wait();
                    updateInverseComputedTask.Wait();
                    return computedTicks;
        }

        public void SendMarketSnapshot(IWebSocketConnection socket, String sourceCurrency, List<String> symbolsToSubscribeTo)
        {
            socket.Send(Util.GenerateMarketSnapshot(sourceCurrency, symbolsToSubscribeTo, lastInstrumentValues));
        }
    }
}
