using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.ServiceRuntime;
using Models.Financial;
using MarketDataPublisher;
using Fleck;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Services;
using FixEngineIngestion;
using MarketDataHandler.Models.Financial;
using System.Threading;
using MessageBus;

namespace MarketStreamingRole
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart()
        {
            Task.Factory.StartNew(InitListener);
          
            return base.OnStart();
        }

        public void InitListener()
        {
            /* ENable for KGI */
            //List<Instrument> symbolList = Util.GetTaggKgiInstruments();

            /* Enable for LMAX */ 
            List<Instrument> symbolList = Util.GetMajorLMAXInstruments();

            RedisTickDataDumper redisDumper = new RedisTickDataDumper();

            List<ITickDataDumper> dataDumpers = new List<ITickDataDumper>() {  redisDumper,  /* new KafkaTickDataDumper() */ };
            
            Console.WriteLine("Initializing Web Socket");

            FleckLog.Level = Fleck.LogLevel.Error;

            MarketDataSocketHandler socketHandler = new MarketDataSocketHandler(symbolList);

            socketHandler.LoadDataFromRedis(redisDumper);

            bool acceptingConnections = false;

            bool isWeekend = false; 

            var server = new WebSocketServer("ws://0.0.0.0:8181");

            //server.Certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2("appservicecertificate.pfx", "5CVnXmEMhOFD3YbafK49WrqQ06pjgNZvIiLcdPBTlot8kJUzG1");

            server.RestartAfterListenError = false;

            server.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    if (!acceptingConnections || isWeekend)
                    {
                        socket.Send(JsonConvert.SerializeObject(MarketDataResponseError.BrokerNotAvailable()));
                    }

                    socketHandler.AddNewSocket(socket);
                };
                socket.OnClose = () =>
                {
                    socketHandler.UnsubscribeFromAll(socket);
                    socketHandler.RemoveSocket(socket);
                };
                socket.OnMessage = message =>
                {
                        MarketDataRequest request;

                        try
                        {
                            request = JsonConvert.DeserializeObject<MarketDataRequest>(message);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Invalid Market Data Request. Couldn't parse JSON Object. Exception :" + ex.Message);
                            socket.Send("Invalid Market Data Request. Couldn't parse JSON Object. Exception :" + ex.Message);
                            return;
                        }

                        if (request.Type == MarketDataRequestType.UNSUBSCRIBE_ALL)
                        {
                            socketHandler.UnsubscribeFromAll(socket).Wait();
                        }
                        else if (request.Type == MarketDataRequestType.SUBSCRIBE)
                        {
                            foreach (var instrument in request.Currencies)
                            {
                                socketHandler.SubscribeSocketToInstrument(socket, instrument.ToUpper()).Wait();
                            }
                        }
                        else if (request.Type == MarketDataRequestType.UNSUBSCRIBE)
                        {
                            foreach (var instrument in request.Currencies)
                            {
                                if (instrument.Contains("/"))
                                {
                                    socketHandler.UnsubscribeSocketFromInstrument(socket, instrument.ToUpper());
                                }
                                else
                                {
                                    List<String> symbolsToUnSubscribeFrom = new List<string>();
                                    List<String> instrumentSymbols = new List<string>();
                                    instrumentSymbols.AddRange(symbolList.Where(x => x.SourceCurrency == instrument).Select(x => x.Symbol));
                                    instrumentSymbols.AddRange(symbolList.Where(x => x.DestinationCurrency == instrument).Select(x => x.InverseSymbol));
                                    symbolsToUnSubscribeFrom.AddRange(instrumentSymbols);

                                    foreach (var unsubscribeFrom in symbolsToUnSubscribeFrom)
                                    {
                                        socketHandler.UnsubscribeSocketFromInstrument(socket, unsubscribeFrom.ToUpper());
                                    }
                                }
                            }
                        }
                        else if (request.Type == MarketDataRequestType.SUBSCRIBE_TO_ALL_FROM_SOURCE)
                        {
                            List<String> symbolsToSubscribeTo = new List<string>();

                            foreach (var instrument in request.Currencies)
                            {
                                List<String> instrumentSymbols = new List<string>();
                                instrumentSymbols.AddRange(symbolList.Where(x => x.SourceCurrency == instrument.ToUpper()).Select(x => x.Symbol));
                                instrumentSymbols.AddRange(symbolList.Where(x => x.DestinationCurrency == instrument.ToUpper()).Select(x => x.InverseSymbol));

                                if (instrument.Count() > 0)
                                {
                                    socketHandler.SendMarketSnapshot(socket, instrument.ToUpper(), instrumentSymbols);
                                    symbolsToSubscribeTo.AddRange(instrumentSymbols);
                                }
                            }

                            foreach (var symbol in symbolsToSubscribeTo)
                            {
                                socketHandler.SubscribeSocketToInstrument(socket, symbol).Wait();
                            }
                        }

                        else if (request.Type == MarketDataRequestType.SOURCE_SNAPSHOT)
                        {
                        foreach (var instrument in request.Currencies)
                        {
                            List<String> symbolsToSubscribeTo = new List<string>();
                            symbolsToSubscribeTo.AddRange(symbolList.Where(x => x.SourceCurrency == instrument.ToUpper()).Select(x => x.Symbol));
                            symbolsToSubscribeTo.AddRange(symbolList.Where(x => x.DestinationCurrency == instrument.ToUpper()).Select(x => x.InverseSymbol));

                            if (symbolsToSubscribeTo.Count() > 0)
                            {
                                socketHandler.SendMarketSnapshot(socket, instrument.ToUpper(), symbolsToSubscribeTo);
                            }
                        }
                        }
                };
            });

            QueueExecutor<MarketTick> queueExec = new QueueExecutor<MarketTick>(x => {

                try
                {
                    List<MarketTick> computedTicks =  socketHandler.PushMarketDataToSubscribedSockets(x);

                    dataDumpers.ForEach(dataDumper => {
                        {
                            try
                            {
                                if (x.MidPrice != 0)
                                {
                                    dataDumper.DumpTick(x);
                                    dataDumper.DumpTickInverse(x);
                                }
                            }
                            catch (Exception) { }
                        }
                    });

                    dataDumpers.ForEach(dataDumper =>
                    {
                        computedTicks.ForEach(
                            tick =>
                            {
                                try
                                {
                                    if (tick != null)
                                    {
                                        if (x.MidPrice != 0)
                                        {
                                            dataDumper.DumpTick(tick);
                                            dataDumper.DumpTickInverse(tick);
                                        }
                                    }
                                }
                                catch (Exception) { }
                            }
                            );

                    });

                }
                catch (Exception) { }
            });

            
            AbstractMarketStreamingService dataStreamer = new LmaxMarketStreamingService(x => { queueExec.QueueItem(x); }, symbolList.Where(x => !x.isComputed).ToList(), newState =>
            {
                if (newState == ConnectionState.CONNECTION_OPEN)
                {
                    acceptingConnections = true;
                }
                else
                {
                    acceptingConnections = false;
                }
            });

            /* Enable for KGI 
             AbstractMarketStreamingService dataStreamer = new FixEngineStreamingService( x =>
            {
                queueExec.QueueItem(x);
            }, symbolList.Where(x => !x.isComputed).ToList(), newState => {
                  if (newState == ConnectionState.CONNECTION_OPEN)
                  {
                    acceptingConnections = true;
                    Console.WriteLine("Now Accepting Connections");
                  }
                  else 
                  {
                    Console.WriteLine("Not Accepting Connections");
                    acceptingConnections = false;
                  }
            } );*/

            ThreadStart ts = new ThreadStart(() => { dataStreamer.StartSession(); });

            Thread t = new Thread(ts);
            t.Start();
            
            while (true)
            {
                bool isWeekendBeforeVal = isWeekend;

                DateTime utcNow = DateTime.UtcNow;
                TimeSpan cutoffEOW = new TimeSpan(21, 00, 00);
                TimeSpan cutoffBOW = new TimeSpan(21, 05, 00);


                if ((utcNow.DayOfWeek == DayOfWeek.Friday && utcNow.TimeOfDay >= cutoffEOW) || (utcNow.DayOfWeek == DayOfWeek.Saturday) || (utcNow.DayOfWeek == DayOfWeek.Sunday && utcNow.TimeOfDay <= cutoffBOW))
                {
                    isWeekend = true;

                    if (isWeekendBeforeVal == false)
                    {
                        socketHandler.SendMarketClosed();
                    }
                }
                else
                {
                    if (isWeekend)
                    {
                        isWeekend = false;
                    }
                }

            }
        }
    }
}
