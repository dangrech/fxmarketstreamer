﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Globalization;

namespace FixEngineIngestion
{
    static class ConfigurationHelper
    {
        public static string GetConfigurationPath()
        {
            AssemblyName assemblyName = Assembly.GetEntryAssembly().GetName();

            string configPath = assemblyName.Name + ".exe.config";

            if (!File.Exists(configPath))
            {
                Configuration cfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                if (!Directory.Exists(Path.GetDirectoryName(configPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(configPath));
                if (File.Exists(cfg.FilePath))
                    File.Copy(cfg.FilePath, configPath);
            }
            return configPath;
        }

        public static Configuration GetConfiguration()
        {
            string configPath = GetConfigurationPath();
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = configPath;

            Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            if (cfg == null)
            {
                throw new ApplicationException("Cannot find configuration file: " + configPath);
            }

            return cfg;
        }

        public static ConfigurationSection GetSection(string name)
        {
            string configPath = GetConfigurationPath();

            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = configPath;

            Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            if (cfg == null)
            {
                throw new ApplicationException("Cannot find configuration file: " + configPath);
            }

            ConfigurationSection section = cfg.GetSection(name);
            if (section == null)
            {
                throw new ApplicationException("Cannot find the '" + name + "' section in the configuration file: " + configPath);
            }
            return section;
        }

        public static string Get(string key)
        {
            string configPath = GetConfigurationPath();

            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = configPath;

            Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            if (cfg == null)
            {
                throw new ApplicationException("Cannot find configuration file: " + configPath);
            }

            KeyValueConfigurationElement elem = cfg.AppSettings.Settings[key];
            if (elem == null)
            {
                throw new ApplicationException("Cannot find the '" + key + "' setting in the configuration file: " + configPath);
            }
            string v = elem.Value;
            if (null == v)
            {
                throw new ApplicationException("Cannot find the '" + key + "' setting in the configuration file: " + configPath);
            }
            return v;
        }

        public static int GetInteger(string key)
        {
            string v = Get(key);
            return int.Parse(v, CultureInfo.InvariantCulture);
        }

        public static double GetDouble(string key)
        {
            string v = Get(key);
            return double.Parse(v, CultureInfo.InvariantCulture);
        }
    }

}