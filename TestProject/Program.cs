﻿using MarketDataHandler.Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessageBus;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            TCPMessageBus<MarketTick> messageBus = new TCPMessageBus<MarketTick>(8081, 8082);

            messageBus.Start();

            TcpClient producer = new TcpClient();
            TcpClient consumer = new TcpClient();

            Task.Run(() => {
                MessageBusClient<MarketTick> messageBusConsumer = new MessageBusClient<MarketTick>("localhost", 8082, 102400);
                messageBusConsumer.Connect();
                messageBusConsumer.Listen(x =>
                {
                    Console.WriteLine(x.ToTaggJsonString());
                });

            });

            Task.Run(() => {
                MessageBusClient<MarketTick> messageBusProducer = new MessageBusClient<MarketTick>("localhost", 8081, 102400);
                messageBusProducer.Connect();
                Random r = new Random();

                while(true)
                {
                    MarketTick tick = new MarketTick("EUR/USD", 10000l, (decimal)r.NextDouble(), (decimal)r.NextDouble(), (decimal)r.NextDouble());
                    messageBusProducer.Publish(tick);
                }
            });

            while (true)
            {
                Thread.Sleep(10000);
            }

        }
    }
}
