using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using MarketDataPublisher;
using MessageBus;
using MarketDataHandler.Models.Financial;

namespace RedisDumperRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("RedisDumperRole is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            catch(Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("RedisDumperRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("RedisDumperRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("RedisDumperRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            RedisTickDataDumper redisDumper = new RedisTickDataDumper();

            List<ITickDataDumper> dataDumpers = new List<ITickDataDumper>() { redisDumper};

            IPEndPoint endpoint = RoleEnvironment.Roles["MessagingBusRole"].Instances[0].InstanceEndpoints["MessageBusSubscribe"].IPEndpoint;

            MessageBusClient<MarketTick> mbClient = new MessageBusClient<MarketTick>(endpoint.Address.ToString(), endpoint.Port, 100000000);

            mbClient.Connect();

            mbClient.Listen(x =>
            {
                dataDumpers.ForEach(outputService => outputService.DumpTick(x));
            });

            while (true)
            {
                if(cancellationToken.IsCancellationRequested)
                {
                    return;
                }
            }
        }
    }
}
