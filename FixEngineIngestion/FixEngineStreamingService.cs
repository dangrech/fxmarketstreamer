﻿using FIXForge.NET.FIX;
using FIXForge.NET.FIX.Scheduling;
using MarketDataHandler.Models.Financial;
using Models.Financial;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MessageBus;

namespace FixEngineIngestion
{
    public class FixEngineStreamingService : AbstractMarketStreamingService
    {
        FixTradingClient client;
        EngineSettings engineSettings;
        SessionConfiguration sessionSettings;
        
        public FixEngineStreamingService(Action<MarketTick> onMarketData, List<Instrument> subscribedInstruments, Action<ConnectionState> onStateChange) : base(onMarketData, subscribedInstruments, onStateChange)
        {
            this.onMarketData = onMarketData;

            EngineSettings globalSettings = new EngineSettings();
            globalSettings.DialectString = OnixHelper.GetDialectString();
            globalSettings.LicenseString = OnixHelper.GetLicenseString();
            globalSettings.ConnectionMode = ConnectionMode.ThreadPool;
            globalSettings.LogInboundMessages = true;
            globalSettings.LogOutboundMessages = true;
            globalSettings.CreateEngineLogFile = true;
            globalSettings.ValidateFieldValues = false;
            globalSettings.ValidateEmptyFieldValues = false;
            globalSettings.ValidateDuplicatedField = false;
            globalSettings.ValidateRepeatingGroupEntryCount = false;
            globalSettings.ValidateRepeatingGroupLeadingTag = false;
            globalSettings.ValidateRequiredFields = false;
            globalSettings.ValidateUnknownFields = false;
            globalSettings.ValidateUnknownMessages = false;
            globalSettings.IgnoreFileBasedStorageIntegrityErrors = true;
            globalSettings.ReceiveBufferSize = 1000000; 

            this.engineSettings = globalSettings;

            Engine.Init(globalSettings);

            /* 
            SessionConfiguration sessionConfig = new SessionConfiguration() { Host = "183.177.33.171" , Port = 23134, SenderCompID= "APIUAT31",
            TargetCompID= "FLEXECNP", Version = ProtocolVersion.FIX42, KeepSequenceNumbersAfterLogout = false, HeartbeatInterval = 60};
            */

            SessionConfiguration sessionConfig = new SessionConfiguration()
            {
                Host = "183.177.35.2",
                Port = 29178,
                SenderCompID = "MK-004",
                TargetCompID = "ONGTYECNP",
                Password = "V8E4rLHeRR",
                Version = ProtocolVersion.FIX42,
                KeepSequenceNumbersAfterLogout = false,
                HeartbeatInterval = 60
            };

            this.sessionSettings = sessionConfig;

            client = new FixTradingClient(sessionConfig, onMarketData, onStateChange);
        }
        
        public override void StartSession()
        {
            client.Connect(subscribedInstruments);
        }

    }
}
