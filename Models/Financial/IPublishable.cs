﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Financial
{
    public interface IPublishable
    {
        String GetUniqueIdentifier();

        String GetPublishableString();

        decimal GetPublishableValue();
    }
}
