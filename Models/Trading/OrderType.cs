﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Trading
{
    public enum OrderType
    {
        MARKET_ORDER = 0, 
        LIMIT_ORDER = 1, 
        STOP_ORDER = 2
    }
}