﻿using Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MarketDataHandler.Models.Financial
{
    public class MarketTick : IPublishable, ISubscribable
    {
        public String Symbol { get; set; }
        public String InverseSymbol { get; set; }
        public String SourceCurrency { get; set;  }
        public String TargetCurrency { get; set;  }
        public long Timestamp { get; set; }
        public decimal AskPrice { get; set; }
        public decimal BidPrice { get; set; }
        public decimal MidPrice { get; set; }
        public decimal InverseAskPrice { get; set; }
        public decimal InverseBidPrice { get; set;  }
        public decimal InverseMidPrice { get; set;  }

        public bool isComputed = false;

        public MarketTick()
        {

        }

        public MarketTick(String instrument, long timestamp, decimal askPrice, decimal bidPrice, decimal displayPrice, bool isComputed = false)
        {
            this.Symbol = instrument;
            this.Timestamp = timestamp;
            this.AskPrice = askPrice;
            this.BidPrice = bidPrice;
            this.MidPrice = displayPrice;
            this.SourceCurrency = instrument.Split('/')[0];
            this.TargetCurrency = instrument.Split('/')[1];
            this.isComputed = isComputed;
            this.InverseSymbol = this.TargetCurrency + '/' + this.SourceCurrency;
            if (this.AskPrice != 0)
            {
                this.InverseAskPrice = 1 / this.AskPrice;
            }
            if (this.BidPrice != 0)
            {
                this.InverseBidPrice = 1 / this.BidPrice;
            }
            if (this.MidPrice != 0)
            {
                this.InverseMidPrice = 1 / this.MidPrice;
            }
        }

      

        public decimal GetPublishableValue()
        {
            return this.MidPrice;
        }
        public String GetUniqueIdentifier()
        {
            return Symbol;
        }

        public ICollection<String> GetSubscribeKeys()
        {
            return new List<String> { this.Symbol, this.SourceCurrency };
        }

        public String GetPublishableString()
        {
            return this.ToAskBidJsonString();
        }

        public String ToAskBidJsonString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"");
            sb.Append(SourceCurrency.ToLower());
            sb.Append("\":{\"");
            sb.Append(TargetCurrency.ToLower());
            sb.Append("\":");
            sb.Append("{\"ask\":");
            sb.Append(AskPrice);
            sb.Append(", \"bid\":");
            sb.Append(BidPrice);
            sb.Append("}");
            sb.Append("}");
            sb.Append("}");
            return sb.ToString();
        }

        public String ToTaggJsonString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"");
            sb.Append(SourceCurrency.ToLower());
            sb.Append("\":{\"");
            sb.Append(TargetCurrency.ToLower());
            sb.Append("\":");
            sb.Append(MidPrice.ToString("0.######"));
            sb.Append("}");
            sb.Append("}");
            return sb.ToString();
        }

        public String InverseToTaggJsonString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"");
            sb.Append(TargetCurrency.ToLower());
            sb.Append("\":{\"");
            sb.Append(SourceCurrency.ToLower());
            sb.Append("\":");
            sb.Append(InverseMidPrice.ToString("0.######"));
            sb.Append("}");
            sb.Append("}");
            return sb.ToString();
        }
    }
}