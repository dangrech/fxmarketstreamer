﻿using MarketDataHandler.Models.Financial;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SocketStreamingRoleStandalone
{
    public class MarketDataSnaphotManager
    {
        ConcurrentDictionary<string, MarketTick> lastPrices;

        public MarketDataSnaphotManager()
        {
            lastPrices = new ConcurrentDictionary<string, MarketTick>();
        }

        public Task Update(MarketTick x)
        {
            return Task.Run(() => { lastPrices.AddOrUpdate(x.GetUniqueIdentifier(), y => x, (key, existing) => x); });
        }

        public String GetSnapshot(string sourceSymbol)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"");
            sb.Append(sourceSymbol.ToLower());
            sb.Append("\":{");

            var keys = lastPrices.Keys.Where(x => x.StartsWith(sourceSymbol.ToUpper()));
            
            for (int i = 0; i < keys.Count(); i ++)
            {
                MarketTick tick;
                lastPrices.TryGetValue(keys.ElementAt(i), out tick);
                sb.Append("\"");
                sb.Append(tick.TargetCurrency.ToLower());
                sb.Append("\" : {\"ask\":");
                sb.Append(tick.AskPrice);
                sb.Append(", \"bid\":");
                sb.Append(tick.BidPrice);
                sb.Append("}");

                if (i != keys.Count() -1)
                {
                    sb.Append(",");
                }
            }

            sb.Append("}");
            sb.Append("}");

            return sb.ToString();
        }
    }
}