﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageBus
{
    public class LmaxInstrumentIdHandler
    {
        private static readonly Dictionary<long, String> InstrumentIdsToSymbol;
        private static readonly Dictionary<String, long> SymbolsToInstrumentIds;

        static LmaxInstrumentIdHandler()
        {
            SymbolsToInstrumentIds = new Dictionary<String, long>() { 
            {"AUD/CAD",100667},
            {"AUD/CHF",	100619},
            {"AUD/JPY",	4008},
            {"AUD/NZD",	100615},
            {"AUD/USD",	4007},
            {"CAD/CHF",	100671},
            {"CAD/JPY",	100669},
            {"CHF/JPY",	4009},
            {"EUR/AUD",	4016},
            {"EUR/CAD",	4015},
            {"EUR/CHF",	4011},
            {"EUR/CZK",	100479},
            {"EUR/DKK",	100485},
            {"EUR/GBP",	4003},
            {"EUR/HKD",	100491},
            {"EUR/HUF",	100497},
            {"EUR/JPY",	4006},
            {"EUR/MXN",	100503},
            {"EUR/NOK",	100509},
            {"EUR/NZD",	100515},
            {"EUR/PLN",	100519},
            {"EUR/RON",	100930},
            {"EUR/RUB",	100679},
            {"EUR/SEK",	100525},
            {"EUR/SGD",	100531},
            {"EUR/TRY",	100537},
            {"EUR/USD",	4001},
            {"EUR/ZAR",	100543},
            {"GBP/AUD",	4017},
            {"GBP/CAD",	4014},
            {"GBP/CHF",	4012},
            {"GBP/CZK",	100481},
            {"GBP/DKK",	100487},
            {"GBP/HKD",	100493},
            {"GBP/HUF",	100499},
            {"GBP/JPY",	4005},
            {"GBP/MXN",	100505},
            {"GBP/NOK",	100511},
            {"GBP/NZD",	100517},
            {"GBP/PLN",	100521},
            {"GBP/SEK",	100527},
            {"GBP/SGD",	100533},
            {"GBP/TRY",	100539},
            {"GBP/USD",	4002},
            {"GBP/ZAR",	100545},
            {"NOK/SEK",	100555},
            {"NZD/CAD",	100673},
            {"NZD/CHF",	100675},
            {"NZD/JPY",	100617},
            {"NZD/SGD",	100677},
            {"NZD/USD",	100613},
            {"USD/CAD",	4013},
            {"USD/CHF",	4010},
            {"USD/CNH",	100892},
            {"USD/CZK",	100483},
            {"USD/DKK",	100489},
            {"USD/HKD",	100495},
            {"USD/HUF",	100501},
            {"USD/ILS",	100927},
            {"USD/JPY",	4004},
            {"USD/MXN",	100507},
            {"USD/NOK",	100513},
            {"USD/PLN",	100523},
            {"USD/RON",	100931},
            {"USD/RUB",	100681},
            {"USD/SEK",	100529},
            {"USD/SGD",	100535},
            {"USD/TRY",	100541},
            {"USD/ZAR",	100547},
            {"GoldSpot",	100637},
            {"GoldSpotMini",	110637},
            {"XAU/EUR",	100925},
            {"XAU/AUD",	100803},
            {"GLD/CNH",	100928},
            {"SilverSpot",	100639},
            {"SilverSpotMini",	110639},
            {"XAG/AUD",	100804},
            {"SLV/CNH",	100929},
            {"XPT/USD",	100806},
            {"XPD/USD",	100807},
            {"UKBrentSpot",	100805},
            {"USCrudeSpot",	100800},
            {"USNaturalGasSpot",	100926},
            {"Australia200",	100888},
            {"Europe50",	100101},
            {"France40",	100099},
            {"Germany30",	100097},
            {"Germany30Mini",	110097},
            {"HongKong50",	100904},
            {"Japan225",	100105},
            {"Spain35",	100906},
            {"UK100",	100089},
            {"USSPX500",	100093},
            {"USSPX500Mini",	110093},
            {"USTech100",	100095},
            {"USTech100Mini",	110095},
            {"WallStreet30",	100091},
            {"WallStreet30Mini",	110091},
            };

            InstrumentIdsToSymbol = SymbolsToInstrumentIds.ToDictionary(x => x.Value, x => x.Key);
        }

        public static long GetInstrumentId(String symbol)
        {
            if (SymbolsToInstrumentIds.ContainsKey(symbol))
            {
                return SymbolsToInstrumentIds[symbol];
            }
            else 
            {
                throw new KeyNotFoundException("No Instrument ID for symbol " + symbol);
            }
        }

        public static String GetSymbol(long instrumentID)
        {
            if (InstrumentIdsToSymbol.ContainsKey(instrumentID))
            {
                return InstrumentIdsToSymbol[instrumentID];
            }
            else
            {
                throw new KeyNotFoundException("No Symbol for Instrument ID " + instrumentID);
            }
        }
    }
}
