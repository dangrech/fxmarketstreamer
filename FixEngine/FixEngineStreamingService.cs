﻿using MarketDataHandler.Models.Financial;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace FixEngine
{
    public class FixEngineStreamingService : AbstractMarketStreamingService
    {
        public FixEngineStreamingService(Action<MarketTick> onMarketData, List<String> subscribedInstruments) : base(onMarketData, subscribedInstruments)
        {

        }

        public override void StartSession()
        {

        }

    }
}
