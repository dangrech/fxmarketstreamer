﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Trading
{
    public class Order
    {
        public Guid Id { get; set;  }
        public OrderType Type { get; set; }
        public OrderState State { get; set; }
        public Symbol Symbol { get; set; }
        public decimal Quantity { get; set; }
        public String Action { get; set; }
        public decimal Limit {get; set; }
        public decimal Stop { get; set; }
        public decimal MarketIfTouched { get; set; }
        public String TIF { get; set; }
        public Order[] OTO { get; set;  }
    }
}