﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Trading
{
    public enum OrderState
    {
        ORDER_PLACED = 0, 
        ORDER_CANCELLED = 1, 
        ORDER_EXECUTED = 2, 
        ORDER_CLOSED = 3 
    }
}