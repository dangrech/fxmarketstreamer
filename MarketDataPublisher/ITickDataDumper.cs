﻿using MarketDataHandler.Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketDataPublisher
{
    public interface ITickDataDumper
    {
        Task DumpTick(MarketTick tick);

        Task DumpTickInverse(MarketTick tick);

//        void DumpSnapshot(Dictionary<string, decimal> lastInstrumentValues);
    }
}
