﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixEngineIngestion
{
    public  class MarketDataUpdate
    {
        public double Bid = 0;
        public double Ask = 0;
        public double BidSize = 0;
        public double AskSize = 0;
    }
}
