﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessageBus
{
    public class MessageBusClient<T>
    {
        String serverUrl;
        int port;
        int incomingBufferSize = 1024;
        bool isConnected = false;
        TcpClient networkClient;
        NetworkStream networkStream;

        public MessageBusClient(String serverUrl, int port, int? incomingBufferSize)
        {
            this.serverUrl = serverUrl;
            this.port = port;
            networkClient = new TcpClient();
            
            if (incomingBufferSize.HasValue)
            {
                this.incomingBufferSize = incomingBufferSize.Value;
            }
        }

        public void Connect()
        {
            networkClient.Connect(serverUrl, port);
            networkStream = networkClient.GetStream();
            isConnected = true;
        }

        public void Publish(T item)
        {
            if(!isConnected)
            {
                throw new Exception("TCP Client isn't connect to server. Make sure you called Connect()");
            }

            Message <T> message = new Message<T>(item);
            Byte[] data = message.GetBytes();
            networkStream.Write(data, 0, data.Length);
        }

        public void Listen(Action<T> onReceive)
        {
            if (!isConnected)
            {
                throw new Exception("TCP Client isn't connect to server. Make sure you called Connect()");
            }

            Byte[] incomingBytes = new Byte[this.incomingBufferSize];
            NetworkStream incomingStream = networkStream;

            int writeIndex = 0;
            int readIndex = 0;

            bool inObject = false;
            uint currentObjectLength = 0;

            while (true)
            {
                    while ((writeIndex += incomingStream.Read(incomingBytes, writeIndex, incomingBytes.Length - writeIndex)) != 0)
                    {
                        if (!inObject && (writeIndex - readIndex) >= 4)
                        {
                            currentObjectLength = BitConverter.ToUInt32(incomingBytes, readIndex);
                            readIndex += 4;
                            inObject = true;
                        }

                        if (inObject && (writeIndex - readIndex) >= currentObjectLength)
                        {
                            Byte[] data = new Byte[currentObjectLength];
                            Array.Copy(incomingBytes, readIndex, data, 0, currentObjectLength);
                            readIndex += (int)currentObjectLength;

                            Message<T> message = new Message<T>(currentObjectLength, data);
                            onReceive(message.GetItem());
                            inObject = false;
                        }

                        if (writeIndex == incomingBytes.Length)
                        {
                            // Got to end of array, to avoid problems compy current temp to beginning of array and start from there again
                            Array.Copy(incomingBytes, readIndex, incomingBytes, 0, writeIndex - readIndex);
                            writeIndex = writeIndex - readIndex;
                            readIndex = 0;
                            inObject = false;
                        }
                    }
                
            }
        }
    }
}
