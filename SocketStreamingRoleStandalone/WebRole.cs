using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Net;
using MessageBus;
using MarketDataHandler.Models.Financial;
using System.Threading;
using MarketDataPublisher;
using Fleck;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace SocketStreamingRoleStandalone
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart()
        {
            return base.OnStart();
        }


        public override void Run()
        {
            MarketDataSnaphotManager snapshotManager = new MarketDataSnaphotManager();

            while (true)
            {
                try
                {
                    IPEndPoint endpoint = RoleEnvironment.Roles["MessagingBusRole"].Instances[0].InstanceEndpoints["MessageBusSubscribe"].IPEndpoint;

                    MessageBusClient<MarketTick> mbClient = new MessageBusClient<MarketTick>(endpoint.Address.ToString(), endpoint.Port, 100000000);

                    mbClient.Connect();

                    WebSocketServer<MarketTick> webSocketServer = new WebSocketServer<MarketTick>();
                    
                    var server = new WebSocketServer("ws://0.0.0.0:8181");

                    //server.Certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2("appservicecertificate.pfx");

                    bool isWeekend = false;

                    server.Start(socket =>
                    {
                        socket.OnOpen = () =>
                        {
                            webSocketServer.AddSocket(socket);
                        };

                        socket.OnClose = () =>
                        {
                            webSocketServer.RemoveSocket(socket);
                        };

                        socket.OnMessage = (message) =>
                        {
                            MarketDataRequest request;

                            try
                            {
                                request = JsonConvert.DeserializeObject<MarketDataRequest>(message);

                            }
                            catch (Exception ex)
                            {
                                socket.Send("{ \"Error\" :  \"Invalid Market Data Request. Couldn't parse JSON Object. Exception :" + ex.Message + "\"}");
                                return;
                            }

                            if(isWeekend)
                            {
                                webSocketServer.SendToSingle(JsonConvert.SerializeObject(MarketDataResponseError.BrokerNotAvailable()), socket);
                            }

                            switch (request.Type){

                                case MarketDataRequestType.SUBSCRIBE:
                                    foreach (var instrument in request.Currencies)
                                    {
                                        webSocketServer.Subscribe(instrument.ToUpper(), socket);
                                    }
                                    break;

                                case MarketDataRequestType.SUBSCRIBE_TO_ALL_FROM_SOURCE:
                                    foreach (var sourceCurrency in request.Currencies)
                                    {
                                        webSocketServer.SendToSingle(snapshotManager.GetSnapshot(sourceCurrency), socket);
                                        webSocketServer.Subscribe(sourceCurrency.ToUpper(), socket);
                                    }
                                    break;

                                case MarketDataRequestType.SOURCE_SNAPSHOT:
                                    foreach(var instrument in request.Currencies)
                                    {
                                        webSocketServer.SendToSingle(snapshotManager.GetSnapshot(instrument), socket);
                                    }
                                    break;

                                case MarketDataRequestType.UNSUBSCRIBE:
                                    foreach (var instrument in request.Currencies)
                                    {
                                        webSocketServer.Unsubscribe(instrument.ToUpper(), socket);
                                    }
                                    break;

                                case MarketDataRequestType.UNSUBSCRIBE_ALL:
                                    webSocketServer.UnsubscribeFromAll(socket);
                                    break;
                            }

                        };
                    });

                    Task.Run(() => {
                       mbClient.Listen(x =>
                       {
                           webSocketServer.Update(x);
                           snapshotManager.Update(x);
                       });
                    });

                    while (true)
                    {
                        bool isWeekendBeforeVal = isWeekend;

                        DateTime utcNow = DateTime.UtcNow;
                        TimeSpan cutoffEOW = new TimeSpan(21, 00, 00);
                        TimeSpan cutoffBOW = new TimeSpan(21, 05, 00);


                        if ((utcNow.DayOfWeek == DayOfWeek.Friday && utcNow.TimeOfDay >= cutoffEOW) || (utcNow.DayOfWeek == DayOfWeek.Saturday) || (utcNow.DayOfWeek == DayOfWeek.Sunday && utcNow.TimeOfDay <= cutoffBOW))
                        {
                            isWeekend = true;

                            if (isWeekendBeforeVal == false)
                            {
                                webSocketServer.SendToAll(JsonConvert.SerializeObject(MarketDataResponseError.BrokerNotAvailable()));
                            }
                        }
                        else
                        {
                            if (isWeekend)
                            {
                                isWeekend = false;
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    Thread.Sleep(10000);
                    Console.WriteLine(ex.Message);
                }

            }
        }
    }
}