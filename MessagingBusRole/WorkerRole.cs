using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using MessageBus;
using MarketDataHandler.Models.Financial;

namespace MessagingBusRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);
        TCPMessageBus<MarketTick> messageBus;


        public override void Run()
        {
            Trace.TraceInformation("MessagingBusRole is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            IPAddress hostname = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MessageBusPublish"].IPEndpoint.Address;
            int listenPort = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MessageBusPublish"].IPEndpoint.Port;
            int outPort = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MessageBusSubscribe"].IPEndpoint.Port;

            messageBus = new TCPMessageBus<MarketTick>(hostname, listenPort, outPort);

            bool result = base.OnStart();

            Trace.TraceInformation("MessagingBusRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("MessagingBusRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("MessagingBusRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            try
            {
                Task t = messageBus.Start();

                t.Wait();

                if (t.Exception != null)
                {
                    throw t.Exception;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
