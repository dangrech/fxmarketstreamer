﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MessageBus
{
    public class Message<T>
    {
        // 4 bytes header 
        private uint Size;
        // object to be transmitted
        private Byte[] Data;

        public Message(T item)
        {
            /* Construct Byte [] Data from item, keeping generic */
            Data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(item));
            Size = (uint) Data.Count();
        }

        public Message(uint Size, Byte [] Data)
        {
            this.Size = Size;
            this.Data = Data;
        }

        public Byte[] GetBytes()
        {
            return BitConverter.GetBytes(Size).Concat(Data).ToArray();
        }

        public T GetItem()
        {
            String json = Encoding.UTF8.GetString(Data);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}