using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using MessageBus;
using MarketDataHandler.Models.Financial;
using System.Net;
using System.Threading.Tasks;

namespace MessageBusRole
{
    public class WebRole : RoleEntryPoint
    {
        TCPMessageBus<MarketTick> messageBus;
        public override bool OnStart()
        {
            IPAddress hostname = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MessageBusPublish"].IPEndpoint.Address;
            int listenPort = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MessageBusPublish"].IPEndpoint.Port;
            int outPort = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MessageBusSubscribe"].IPEndpoint.Port;

            messageBus = new TCPMessageBus<MarketTick>(hostname, listenPort, outPort);

            return base.OnStart();
        }

        public override void Run()
        {
            try
            {
                Task t = messageBus.Start();

                t.Wait();

                if (t.Exception != null)
                {
                    throw t.Exception;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            base.Run();
        }

    }
}
