﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketDataPublisher
{
    public class MarketDataResponseError
    {
        public String Error;

        private MarketDataResponseError() { }

        public static MarketDataResponseError BrokerNotAvailable()
        {
            MarketDataResponseError error = new MarketDataResponseError();
            error.Error = "MARKET_DATA_NOT_AVAILABLE";
            return error;
        }


    }
}
