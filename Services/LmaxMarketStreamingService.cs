﻿using Com.Lmax.Api;
using Com.Lmax.Api.Order;
using Com.Lmax.Api.OrderBook;
using MarketDataHandler.Models.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using MessageBus;

namespace Services
{
    public class LmaxMarketStreamingService : AbstractMarketStreamingService
    {
        private string lmaxLiveUrl = @"https://api.lmaxtrader.com";
        private string lmaxUsername = @"sixcapitalsub6";
        private string lmaxPassword = @"6hippodata!";
        private ISession currentSession;
        private List<long> instrumentIds;
        Action<ConnectionState> onStateChanged;


        public LmaxMarketStreamingService(Action<MarketTick> onMarketData, List<Models.Financial.Instrument> subscribedInstruments, Action<ConnectionState> onStateChanged) : base(onMarketData, subscribedInstruments, onStateChanged)
        {
            this.instrumentIds = new List<long>();
            foreach (var instrument in subscribedInstruments)
            {
                this.instrumentIds.Add(LmaxInstrumentIdHandler.GetInstrumentId(instrument.Symbol));
            }

            this.onStateChanged = onStateChanged;
        }

        public override void StartSession()
        {
            LmaxApi lmaxApi = new LmaxApi(lmaxLiveUrl);

            LoginRequest loginRequest = new LoginRequest(lmaxUsername, lmaxPassword, ProductType.CFD_LIVE);
            
            lmaxApi.Login(loginRequest, LmaxLoginCallback, x=> onSessionDisconnected(x.Exception));
        }

        private void startListening()
        {
            onStateChanged(ConnectionState.CONNECTION_OPEN);
            currentSession.Start();
        }

       private void LmaxLoginCallback(ISession session)
        {
            onStateChanged(ConnectionState.CONNECTION_PENDING); 
            currentSession = session;
            currentSession.MarketDataChanged += this.OnOrderBookEvent;
            currentSession.EventStreamSessionDisconnected += this.onSessionDisconnected;
            currentSession.EventStreamFailed += this.onSessionDisconnected;
            
            foreach (var instrumentId in instrumentIds)
            {
                currentSession.Subscribe(new OrderBookSubscriptionRequest(instrumentId),
                                       () => Console.WriteLine("Subscribed to instrument: " + instrumentId),
                                       failureResponse =>
                                       {
                                           Console.WriteLine("Failed to subscribe to instrument: " +
                                                             instrumentId);
                                           throw new Exception("Error subscribing to instrument: " +
                                                               instrumentId);
                                       });
            }
            startListening();
        }

        private void onSessionDisconnected()
        {
            onStateChanged(ConnectionState.CONNECTiON_CLOSED);
            Console.WriteLine("Session Disconnected. Attempt to reconnect");

            StartSession();
        }
        private void onSessionDisconnected(Exception ex)
        {
            onStateChanged(ConnectionState.CONNECTiON_CLOSED);
            Console.WriteLine("Session Disconnected. Attempt to reconnect");
            
            StartSession();
        }

        private void OnOrderBookEvent(OrderBookEvent orderBookEvent)
        {
            if (orderBookEvent.AskPrices.Count > 0 || orderBookEvent.BidPrices.Count > 0)
            {
                base.onMarketData(new MarketTick(LmaxInstrumentIdHandler.GetSymbol(orderBookEvent.InstrumentId), orderBookEvent.Timestamp,
                    orderBookEvent.AskPrices[0].Price, orderBookEvent.BidPrices[0].Price,
                    (orderBookEvent.AskPrices[0].Price + orderBookEvent.BidPrices[0].Price) / 2));
            }
        }

        private OnFailure LmaxLoginFailureCallback(String reason)
        {
            onStateChanged(ConnectionState.CONNECTiON_CLOSED);
            Console.WriteLine("Login Failed.");
            return null;
        }
    }
}