﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDataHandler.Models.Trading
{
    public class AccountBalance
    {
        public decimal Liquidity { get; set; }
        public decimal Cash { get; set;  }
        public decimal ProfitLoss { get; set; }
        public decimal UnsettledCash { get; set;  }
        public decimal BuyingPower { get; set;  }
    }
}